<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class ConvertController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function convert(Request $request)
    {
        $params = $this->validate($request, [
            "file" => "required|mimes:docx"
        ]);

        $uuid = Uuid::uuid4();
        $doctoPath = Storage::path('docto.exe');
        $file = Storage::put("/", $request->file);
        $getFullPath = Storage::path($file);
        $output = Storage::path("{$uuid}.pdf");
        $cmd = "$doctoPath -WD -f $getFullPath -o $output -t wdFormatPDF";
        exec($cmd, $result, $resultCode);
        if ($resultCode == 0) {
            Storage::delete($file);
        }
        return Storage::download("$uuid.pdf");
    }
}
